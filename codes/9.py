#!/usr/bin/env python3

from scipy import signal
import numpy as np
import matplotlib
matplotlib.use('PDF')  # noqa
from matplotlib import pyplot as plt

plt.rc('text', usetex=True)
plt.rc('font', family='serif')


def plot_H(Hs, worN):
    xticks_num = 9
    fig = plt.figure(figsize=plt.figaspect(0.5) * 0.7)
    axis = (fig.add_subplot(121), fig.add_subplot(122))
    for axes in axis:
        axes.set_xlabel(r"$\omega$")
        axes.set_xticks(np.linspace(0, worN, xticks_num))
        axes.set_xticklabels(np.array([r"$\frac{%i}{%i}\pi$" % (i / np.pi * (xticks_num -1) / 2, (xticks_num -1) / 2)
                                     for i in np.linspace(0, 2 * np.pi, xticks_num)]))
    axis[0].set_ylabel(r"$|H(\omega)|$")
    axis[0].set_title(r"$|H(\omega)|$")
    axis[1].set_ylabel(r"$\angle H(\omega)$")
    axis[1].set_title(r"$\angle H(\omega)$")
    fig.suptitle(r"$H(\omega)$")
    for i in range(len(Hs)):
        axis[0].plot(np.abs(Hs[i]), label=(r"$H_%i$" % (i + 1)))
        axis[1].plot(np.angle(Hs[i]), label=(r"$H_%i$" % (i + 1)))
    for axes in axis:
        axes.legend()
    fig.set_tight_layout(True)
    fig.savefig("../results/9-1")
    # plt.show()


def plot_ys(x, ys):
    fig = plt.figure(figsize=plt.figaspect(1.3) * 1.3)
    ax = fig.add_subplot(len(ys) + 1, 1, 1)
    ax.stem(np.tile(x, 6), label=r"$x[n]$")
    ax.set_xticks(np.arange(0, 121, 10))
    ax.set_xticklabels(np.arange(-20, 101, 10))
    ax.set_xlabel(r"$n$")
    ax.set_ylabel(r"$x[n]$")
    ax.set_title(r"$x[n]$")
    for i in range(len(ys)):
        ax = fig.add_subplot(len(ys) + 1, 1, i + 2)
        ax.stem(np.tile(ys[i], 6), label=(r"$y_%i[n]$" % (i + 1)), linefmt="C%i-" % (i), markerfmt="C%io" % (i))
        ax.stem(np.tile(x, 6), label=r"$x[n]$", linefmt="C%i:" % (len(ys)), markerfmt="C%i." % (len(ys)))
        ax.set_xticks(np.arange(0, 121, 10))
        ax.set_xticklabels(np.arange(-20, 101, 10))
        ax.set_xlabel(r"$n$")
        ax.set_title(r"$y_%i[n]$" % (i + 1))
        ax.legend()
    fig.set_tight_layout(True)
    fig.savefig("../results/9-3")
    # plt.show()


def plot_a(a):
    fig = plt.figure(figsize=plt.figaspect(0.3) / 3)
    ax = fig.gca()
    ax.stem(a)
    ax.set_xticks(np.arange(len(a)))
    ax.set_xlabel(r"$k$")
    ax.set_ylabel(r"$a_k$")
    ax.set_title(r"$a_k$")
    fig.set_tight_layout(True)
    fig.savefig("../results/9-2")
    # plt.show()


def plot_as(a, bs):
    fig = plt.figure(figsize=plt.figaspect(0.3) / 2)
    ax = fig.gca()
    ax.stem(a, label="")
    for i in range(len(bs)):
        ax.stem(bs[i], label=(r"$y_%i[n]$" % (i + 1)), linefmt="C%i-" % (i), markerfmt="C%io" % (i))
    ax.stem(a, label=r"$x[n]$", linefmt="C%i-" % (len(ys)), markerfmt="C%io" % (len(ys)))
    ax.set_xticks(np.arange(len(a)))
    ax.set_xlabel(r"$k$")
    ax.set_title(r"$a_k$")
    ax.legend(loc=4)
    ax.grid()
    fig.set_tight_layout(True)
    fig.savefig("../results/9-4")
    # plt.show()


if __name__ == '__main__':
    coefficients = []
    coefficients.append(([1, -0.8], [1]))
    coefficients.append(([1, +0.8], [1]))

    worN = 2014
    H = []
    for coefficient in coefficients:
        H.append(signal.freqz(*reversed(coefficient), worN=worN, whole=True)[1])
    plot_H(H, worN)

    n = 20
    a = np.array([
        3 / 4 if (k if k <= n / 2 else n - k) in [-1, +1] else
        -1 / 2 if (k if k <= n / 2 else n - k) in [-9, +9] else
        0
        for k in range(n)
    ])
    plot_a(a)

    x = np.fft.ifft(a) * n

    ys = []
    for coefficient in coefficients:
        ys.append(signal.lfilter(*reversed(coefficient), x))
    plot_ys(x, ys)
    plot_as(a, [np.fft.fft(y) for y in ys])
