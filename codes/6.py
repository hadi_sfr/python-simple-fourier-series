#!/usr/bin/env python3

import numpy as np
import matplotlib
matplotlib.use('PDF')  # noqa
from matplotlib import pyplot as plt

plt.rc('text', usetex=True)
plt.rc('font', family='serif')


def delta(t, n):
    return (t == n) * 1


def step(t, n):
    return (t >= n) * 1


def plot(x, h, y, t, subname):
    def plt_axis(sig, name, ax):
        ax.stem(t[name], sig)
        ax.set_xlabel(r"$n$")
        ax.set_ylabel(r"$%s[n]$" % name)

    fig = plt.figure()

    plt_axis(x, "x", fig.add_subplot(221))
    plt_axis(h, "h", fig.add_subplot(222))
    ax = fig.add_subplot(212)
    plt_axis(y, "y", ax)
    ax.set_title(r"$y[n] = x[n] \ast h[n]$")

    # fig.suptitle("Convolution")
    fig.set_tight_layout(True)
    fig.savefig("../results/6-%s" % subname)
    # plt.show()


def limited_example():
    t = dict()
    t["h"] = np.arange(-1, 1 + 1)
    t["x"] = np.arange(0, 2 + 1)
    t["y"] = np.arange(-1, 3 + 1)
    h = 2 * delta(t["h"], -1) - 2 * delta(t["h"], 1)
    x = delta(t["x"], 0) + delta(t["x"], 2)
    y = np.convolve(x, h)
    plot(x, h, y, t, 1)


def limited(a, b, c, d):
    assert(a <= b)
    assert(c <= d)
    t = dict()
    t["h"] = np.arange(a, b + 1)
    t["x"] = np.arange(c, d + 1)
    t["y"] = np.arange(a + c, b + d + 1)
    h = delta(t["h"], a) + delta(t["h"], b)
    x = delta(t["x"], c) + delta(t["x"], d)
    y = np.convolve(x, h)
    plot(x, h, y, t, 2)


def unlimited():
    t = dict()
    t["h"] = np.arange(-3, 14 + 1)
    t["x"] = np.arange(0, 24 + 1)
    t["y"] = np.arange(-3 + 0, 14 + 24 + 1)
    h = step(t["h"], -2)
    x = step(t["x"], 2) * (1 / 2) ** (t["x"] - 2)
    y = np.convolve(x, h)
    plot(x, h, y, t, 3)


if __name__ == '__main__':
    limited_example()
    limited(0, 3 - 1, 0, 4 - 1)
    unlimited()
