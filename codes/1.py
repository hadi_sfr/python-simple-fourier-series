#!/usr/bin/env python3

import numpy as np
import matplotlib
matplotlib.use('PDF')  # noqa
from matplotlib import pyplot as plt

plt.rc('text', usetex=True)
plt.rc('font', family='serif')


if __name__ == '__main__':
    x = []
    labels = []

    n = np.array(range(0, 32))
    labels.append(r"$sin(\pi n / 4)$")
    x.append(np.sin(np.pi * n / 4) ** 2)
    labels.append(r"$sin(\pi n / 4) \cdot cos(\pi * n / 8)$")
    x.append(np.sin(np.pi * n / 4) * np.cos(np.pi * n / 8))
    n = np.array(range(0, 24))
    N = 6
    labels.append(r"$cos(\frac{2 \pi n}{%i}) + 3 sin(\frac{5 \pi n}{2 * %i})$" % (N, N))
    x.append(np.cos(2 * np.pi * n / N) + 3 * np.sin(5 * np.pi * n / 2 / N))

    fig, axes = plt.subplots(3)
    # fig.suptitle(r"Discrete-time $sin$ Signals")
    for i in range(len(axes)):
        axes[i].stem(x[i])
        axes[i].set_title(labels[i])
        axes[i].set_xlabel(r"$n$")
        axes[i].set_ylabel(r"$x_%i[n]$" % (i + 1))
    fig.set_tight_layout(True)
    fig.savefig("../results/1")
    # plt.show()
