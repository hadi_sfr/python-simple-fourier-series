#!/usr/bin/env python3

import numpy as np
import matplotlib
matplotlib.use('PDF')  # noqa
from matplotlib import pyplot as plt

plt.rc('text', usetex=True)
plt.rc('font', family='serif')


def plot(prop, subname, title, t, g):
    fig = plt.figure()

    for i in range(len(g)):
        ax = fig.add_subplot(2, 2, i + 1)
        ax.plot(t, g[i])
        ax.set_xlabel(r"$a$")
        ax.set_ylabel(r"$%s_a\{g_%i\}$" % (prop, i + 1 if i < 3 else i + 2))
        ax.set_title(r"$g_%i%s$" % (i + 1 if i < 3 else i + 2, "(t)" if i < 3 else "[n]"))
        ax.grid()

    fig.suptitle(title)
    fig.set_tight_layout(True)
    fig.savefig("../results/5-%s" % subname)
    # plt.show()


if __name__ == '__main__':
    t = np.arange(0, 30, 0.1)
    g = []
    g.append(t + 5 / 2 / np.pi * np.sin(np.pi * 2 / 5 * t))
    g.append(8 * t)
    g.append(np.array([
        3 / 2 + i * i / 2 if i >= 1 else
        i + i * i
        for i in t
    ]))
    g.append(16 + 0 * t)
    plot("E", 1, "Energy", t, g)

    t = np.arange(0.1, 60, 0.1)
    g = []
    g.append(1 / 2 + 5 / 4 / np.pi / t * np.sin(np.pi * 5 / 2 * t))
    g.append(4 + 0 * t)
    g.append(np.array([
        3 / 4 / i + i / 4 if i >= 1 else
        1 / 2 + i / 2
        for i in t
    ]))
    g.append(8 / t)
    plot("P", 2, "Power", t, g)
