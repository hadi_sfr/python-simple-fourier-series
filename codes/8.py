#!/usr/bin/env python3

import numpy as np
import matplotlib
matplotlib.use('PDF')  # noqa
from matplotlib import pyplot as plt

plt.rc('text', usetex=True)
plt.rc('font', family='serif')


def prepare_axis(ax, title, x_max, x_label=r"$k$", x_step=4):
    ax.set_xlabel(x_label)
    ax.set_ylabel(title)
    ax.set_title(title)
    ax.set_xticks(range(0, x_max + 1, x_step))


if __name__ == '__main__':
    x = []
    x.append(np.tile(1, 8))
    x.append(np.concatenate([np.tile(1, 8), np.tile(0, 15 - 7)]))
    x.append(np.concatenate([np.tile(1, 8), np.tile(0, 31 - 7)]))

    a = []
    for s in x:
        a.append(np.fft.fft(s) / len(s))

    for i in range(len(x)):
        fig, axes = plt.subplots(2, 2)
        y_axis_length = 64
        assert(y_axis_length % len(x[i]) == 0)
        axes[0, 0].stem(np.tile(x[i], y_axis_length // len(x[i])))
        prepare_axis(axes[0, 0], r"$x_%i[n]$" % (i + 1), y_axis_length, r"$n$", 8)
        axes[0, 1].stem(np.abs(a[i]))
        prepare_axis(axes[0, 1], r"$|a_{k_%i}|$" % (i + 1), len(a[i]))
        axes[1, 0].stem(np.real(a[i]))
        prepare_axis(axes[1, 0], r"$Re\{a_{k_%i}\}$" % (i + 1), len(a[i]))
        axes[1, 1].stem(np.imag(a[i]))
        prepare_axis(axes[1, 1], r"$Im\{a_{k_%i}\}$" % (i + 1), len(a[i]))
        print("a_0_%i = %s" % (i + 1, a[i][0]))
        fig.suptitle(r"Fourier Series of $x_%i[n]$" % (i + 1))
        fig.set_tight_layout(True)
        fig.savefig("../results/8-%i" % (i + 1))
        # fig.show()

    fig, axes = plt.subplots(2, 2)
    fig.suptitle(r"Fourier Series of $x_%i[n]$" % len(x))
    ax = axes[1, 1]
    # ax.stem(np.fft.ifft(a[-1]) * len(x[-1]))
    prepare_axis(ax, r"$x_{%i_{%s}}[n]$" % (len(x), "all"), len(x[-1]), x_label=r"$n$")
    a = np.concatenate([a[-1][len(a[-1]) // 2 + 1:], a[-1][:len(a[-1]) // 2 + 1]])
    k = np.arange(-len(a) // 2 + 1, len(a) // 2 + 1)
    ax.stem(np.array([sum(
        a[np.nonzero(k == -15)[0][0]:np.nonzero(k == 16)[0][0] + 1]
        * np.exp(n * k[np.nonzero(k == -15)[0][0]:np.nonzero(k == 16)[0][0] + 1] * np.pi * 2j / len(a))
    ) for n in np.arange(0, 32)]))
    for i in range(3):
        l = [2, 8, 12][i]
        ax = axes[i // 2, i % 2]
        ss = 0
        ax.stem(np.array([sum(
            a[np.nonzero(k == -l)[0][0]:np.nonzero(k == l)[0][0] + 1]
            * np.exp(n * k[np.nonzero(k == -l)[0][0]:np.nonzero(k == l)[0][0] + 1] * np.pi * 2j / len(a))
        ) for n in np.arange(0, 32)]))
        prepare_axis(ax, r"$x_{%i_{%s}}[n]$" % (len(x), l), len(x[-1]), x_label=r"$n$")
    ax = axes[1, 1]
    fig.set_tight_layout(True)
    fig.savefig("../results/8-%i" % (len(x) + 1))
    # fig.show()
