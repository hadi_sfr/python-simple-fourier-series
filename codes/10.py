#!/usr/bin/env python3

from datetime import datetime
import numpy as np
from random import randint


def dtfs(x):
    return np.array([sum(x * np.exp(-2j * k * np.pi * np.arange(len(x)) / len(x))) / len(x) for k in range(len(x))])


if __name__ == '__main__':
    for n in [10, 100, 1000, 10000]:
        x = np.array([randint(0, 100) for i in range(n)])
        runtime = {}
        start = datetime.now()
        fft_coefs = np.fft.fft(x) / n
        runtime["fft"] = datetime.now() - start
        start = datetime.now()
        dtfs_coefs = dtfs(x)
        runtime["dtfs"] = datetime.now() - start
        assert(all(np.abs(fft_coefs - dtfs_coefs) < 0.0001))
        print("n = %5i\t dtfs / fft = %5i (%s)" % (n, runtime["dtfs"] / runtime["fft"], runtime))
