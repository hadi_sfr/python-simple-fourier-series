#!/usr/bin/env python3

from math import ceil
from random import randint
import numpy as np
import matplotlib
matplotlib.use('PDF')  # noqa
from matplotlib import pyplot as plt

plt.rc('text', usetex=True)
plt.rc('font', family='serif')


def delta(t, n):
    return (t == n) * 1


def step(t, n):
    return (t >= n) * 1


def manual_block_convolution(x, h):
    xs = []
    xs.append(x[0:50])
    xs.append(x[50:100])
    ys = [np.convolve(x, h) for x in xs]
    y = np.zeros(109)
    y += np.concatenate([ys[0], np.zeros(50)])
    y += np.concatenate([np.zeros(50), ys[1]])
    return y


def general_block_convolution(x, h, l):
    xs = [x[i * l:(i + 1) * l] for i in range(ceil(len(x) / l))]
    ys = [np.convolve(x, h) for x in xs]
    y = np.zeros(len(x) + len(h) - 1)
    for i in range(ceil(len(x) / l)):
        y += np.concatenate([np.zeros(i * l), ys[i], np.zeros(len(x) + len(h) - 1 - (i * l) - len(ys[i]))])
    return y


if __name__ == '__main__':
    n = {
        "x": np.arange(100),
        "h": np.arange(10),
    }
    h = (0.9) ** n["h"]
    x = np.cos(n["x"] * n["x"]) * np.sin(2 * np.pi / 5 * n["x"])

    fig = plt.figure()
    ax = fig.gca()
    ax.stem(n["x"], x, label=r"$x[n] = \cos(n^2) \cdot \sin(\frac{2 \pi}{5} n)$", linefmt="C0:", markerfmt="C0,")
    ax.stem(n["h"], h, label=r"$h[n] = (0.9)^ n \big(u[n] - u[n - 10]\big)$", linefmt="C1-", markerfmt="C1o")
    ax.stem(n["x"], np.convolve(x, h)[0:100],
            label=r"$y[n] = x[n] \ast h[n]$ (\texttt{np.convolve})",
            linefmt="C6-", markerfmt="C6+")
    ax.stem(n["x"], manual_block_convolution(x, h)[0:100],
            label=r"$y[n] = x[n] \ast h[n]$ (\texttt{manual\_block\_convolution})",
            linefmt="C4-.", markerfmt="C4x")
    ax.stem(n["x"], general_block_convolution(x, h, 50)[0:100],
            label=r"$y[n] = x[n] \ast h[n]$ (\texttt{general\_block\_convolution(\textit{50})})",
            linefmt="C2:", markerfmt="C2.")
    ax.stem(n["x"], general_block_convolution(x, h, randint(len(h) + 1, len(x)))[0:100],
            label=r"$y[n] = x[n] \ast h[n]$ (\texttt{general\_block\_convolution(\textit{rand})})",
            linefmt=" ", markerfmt="C8,")
    ax.set_xlabel(r"$n$")
    ax.set_ylabel(r"$y[n]$")
    ax.set_yticks(range(-4, 3))
    ax.set_xticks(range(0, 101, 5))
    ax.grid()
    ax.legend()
    ax.set_title(r"Block Convolution")
    fig.set_tight_layout(True)
    fig.savefig("../results/7")
    # plt.show()
