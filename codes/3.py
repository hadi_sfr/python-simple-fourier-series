#!/usr/bin/env python3

import numpy as np
import matplotlib
matplotlib.use('PDF')  # noqa
from matplotlib import pyplot as plt

plt.rc('text', usetex=True)
plt.rc('font', family='serif')


def linearity():
    def system(x):
        return np.sin((np.pi / 2) * x)

    t = np.array(range(-2, 3))
    x = []
    labels = []
    x.append(np.array([int(n == 0) for n in t]))
    labels.append(r"$\delta[n]$")
    x.append(x[0] * 2)
    labels.append(r"$2\delta[n]$")

    fig = plt.figure()
    for i in [0, 1]:
        ax = fig.add_subplot(2, 2, i + 1)
        ax.stem(x[i])
        ax.set_xlabel(r"$n$")
        ax.set_ylabel(r"$x_%s[n]$" % str(i + 1))
        ax.set_xticks(range(0, len(t)))
        ax.set_xticklabels(t)
        ax.set_yticks(range(0, 3))
        ax.set_title(labels[i])
    ax = fig.add_subplot(212)
    ax.stem(t, system(x[0] + x[1]), label=r"$S\{x_1 + x_2\}$", linefmt="C2-", markerfmt="C2x")
    ax.stem(t, system(x[0]) + system(x[1]), label=r"$S\{x_1\} + S\{x_2\}$", markerfmt="C0+")
    ax.legend(loc=5)
    ax.set_xlabel(r"$n$")
    ax.set_xticks(t)
    ax.set_title(r"$S: y[n] = \sin(\frac{\pi}{2}x[n])$")
    fig.suptitle(r"Linearity")
    fig.set_tight_layout(True)
    fig.savefig("../results/3-1")


def causality():
    def system(x):
        return (np.concatenate([np.zeros(1), x])  # x[n]
                + np.concatenate([x, np.zeros(1)]))  # x[n + 1]

    t = [np.array(range(-5, 10)), np.array(range(-6, 10))]
    x = np.array([int(n >= 0) for n in np.array(range(-5, 11))])

    fig = plt.figure(figsize=plt.figaspect(0.3) / 2)
    ax = plt.gca()
    ax.stem(t[0], x[:-1], label=r"$x[n]$", markerfmt="C0o")
    ax.stem(t[1], system(x)[:-1], label=r"$y[n] = x[n] + x[n + 1]$", linefmt="C1-", markerfmt="C1.")
    ax.set_xlabel(r"$n$")
    ax.legend()
    ax.set_title(r"Causality of $S: y[n] = x[n] + x[n + 1]$")
    fig.set_tight_layout(True)
    fig.savefig("../results/3-2")


def boundedness():

    t = np.array(range(-5, 30))
    x = np.array([1 / np.abs(n) if n != 0 else 1 for n in t])

    fig = plt.figure(figsize=plt.figaspect(0.3) / 2)
    ax = plt.gca()
    ax.stem(t, x, label=r"$x[n] = \delta[n] + \frac{1 - \delta[n]}{|n|}$", markerfmt="C0.")
    ax.stem(t, np.log(x), label=r"$y[n] = \log(x[n])$", linefmt="C1-", markerfmt="C1o")
    ax.set_xlabel(r"$n$")
    ax.legend()
    ax.set_title(r"Boundedness of $S: y[n] = \log(x[n])$")
    fig.set_tight_layout(True)
    fig.savefig("../results/3-3")


if __name__ == '__main__':
    linearity()
    causality()
    boundedness()
